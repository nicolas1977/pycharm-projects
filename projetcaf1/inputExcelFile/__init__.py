import numpy as np
import pandas as pd
import requests as rq
from pandas import ExcelWriter
import xlsxwriter
import xlrd


def main():
	try:
		# code(4)
		# code(4)
		A = np.full((5, 6), 1, dtype=np.int, order='F')
		B = np.zeros((5, 6), dtype=np.int, order='F')

		i: int = 0
		j: int = 0
		while (i < len(A)):
			while (j < len(A)):
				A[i][j] = i ^ 2 + j ^ 3
				j = j + 1
			# End Loop 1
			i = i + 1
			j = 0
		# End Loop 2

		print(A)

		print("assalam ei com")

		# Create a Pandas Excel writer using XlsxWriter as the engine
		df1 = pd.DataFrame(A,
						   index=['day1', 'day2', 'day3', 'day4', 'day5'],
						   columns=['com1', 'com2', 'com3', 'com4', 'com5', 'com6']
						   )
		print(df1)

		# Create a Pandas Excel writer using XlsxWriter as the engine.
		# in the same directory of folder by defaults
		wt = pd.ExcelWriter('excel_panda.xlsx', engine='xlsxwriter')
		# visualisez label index=(true or false)
		df1.to_excel(wt, sheet_name='sample_sheet1', index=True)
		wt.save()

		df2 = pd.read_excel('excel_panda.xlsx', sheet_name=0)
		assert isinstance(df2, object)
		print(df2)

		#Differenc access data from Excel files
		print(df2.iat[0,5])
		print(df2.loc[0][0])
		print(df2.loc[0][4])
		print(df2.iloc[-2][4])

		i: int = 0
		j: int = 1
		#Assign DataFrame from Multi Dimentional Array
		while (i < len(B)):
			while (j < len(B)+2):
				B[i][j-1] = df2.iat[i,j]
				j = j + 1
			# End Loop 1
			i = i + 1
			j = 1
		# End Loop 2

		print(B)

	except ImportError:
		print('founded error')


# EndTry


# endprogramMain()

if __name__ == '__main__':  # the starting step of main program
	main()
# EndIf
