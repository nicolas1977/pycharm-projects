import numpy as np

global i
global j
global k

class Letters():
    A=np.zeros((10,5)) #for assignement number of key
    B=np.chararray((100,3),itemsize=1) #for assignement letter empty array 2 dimentional
    #A(i,0) alphabet; A(i,1) entry level, A(i,2) output
    #calling this subroutine derived type fiels , return an alphabet in array B 0-rank
    def make_alpha(self,B):
        #print(B)
        alpha='ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ'
        i=-1
        while(True):
            i=i+1
            if(i>=52):
                break
            else:
                B[i][0]=alpha[i]
                #tranfert of alphabet in array B rank-0
            #EndIf
        return(B)
    #
    def make_input(self,B,ch):
        i=-1
        while(True):
            i=i+1
            if(i>=len(ch)):
                break
            else:
                B[i][1]=ch[i]
                #tranfert of input user in array B rank-1
            #EndIf
        #EndWhile
        return(B)

    #End_Funciotn_make_aplha
#End_Class




def main():
    try:
        # creating an empty array two dimentional
        #A = np.chararray((10, 2, 2), itemsize=15)  # the first rank of two dimentionnaly is the keys and the second value is
        #A[:] = '' #empty array type character
        #print(A)
        # the value_the signification of one mots
        cy=Letters() #istancement of object of data structured derived type class Letters
        cy.B[:] = ' '
        #make alpha to variable structure array derived type class function field
        cy.make_alpha(cy.B)
        #print(cy.B)
        cha=str(input('enter à phrase please:_'))
        #variable object subroutine field with template class Letter to assignement a cyc the string input
        cy.make_input(cy.B, cha)
        k = int(input('enter à key of decalage:_'))
        #passing in dummy argument all object class
        cy=crypt(cha,k,cy) #assignement intierely array after  numerical calcul
        #into the cy.B array in main program
        #print(cy.B)
        output_array(cha, cy)
        #decryptage of string part of main program
        #reinitialised array cy.B for remake and input new string
        cy.B[:] = ' '
        #make alpha to variable structure array derived type class function field
        cy.make_alpha(cy.B)
        cha = str(input('enter à phrase to decrypter:_'))
        #variable object subroutine field with template class Letter to assignement a cyc the string input
        cy.make_input(cy.B, cha)
        #print(cy.B)
        #calling function of decryptage
        decrypte(cha,cy)
        #
        print('avec quels clef (key english) le message vous semble plus')
        print('humainement lisible aux humains_?')
    #dans la cas il y a un erreur the program passe in this step
    except ImportErro:
        print('founded error')
    #EndTry
#EndMainFunctio
#
#
def output_array(ch_d, cy):
    #print(ch_d)
    #print(cy)
    i=-1
    nes_cha = ''
    while (True):
        i = i + 1
        if (i >= len(ch_d)):
            break
        else:
            if(cy.B[i][2]==''):
                nes_cha = str(nes_cha) + '_'
            else:
                # char = cy.B[i][2]
                nes_cha = str(nes_cha) + str(cy.B[i][2])[2]
            # EndIf
        # EndIf
        # print(nes_cha)
    # EndWhile
    print('the crypted or decripted phrase that you are saisi is_:')
    print(nes_cha)

#Endfunction_output_array
#
#
def crypt(ph,ke,cyc):
    print(cyc.B)
    i=-1
    while(True):
        i=i+1
        if(i>len(ph)):
            break
        else:
            j=-1
            while(True):
                j = j + 1
                if(j>=26):
                    break
                elif(cyc.B[i][1].upper()==cyc.B[j][0] and cyc.B[i][1]!=''):
                    #cyc.B(i,1) is user data class variable object array rank 2
                    #in the hyhoteses of not spacing
                    #assignement to rank output B(i,2) rank 2
                    #the decalated input of user by key ke
                    #mod26 the rest of division on 26
                    cyc.B[i][2]=cyc.B[j+ke%26][0]
                elif(cyc.B[i][1]==''):
                    #in the hyhoteses of spacing
                    cyc.B[i][2] = ' ' #cyc.B[i][1] THE spacing is '' for python reduced to empty
                    #not need to continue to analylsed another letters
                    break
                #EndIf
            #EndWhile
            #print(cyc.B)
        #EndIf
    #EndWhile
    return(cyc)
#EndFunction_Crypt
#
def decrypte(ph,cyc_d):
    #dummy arguments phrase a decripter, key and multi dimentional data structured variable object array fields cyc
    print(cyc_d.B)
    i=-1
    k=0
    while(True):
        k=k+1
        if(k>=26):
            break
        else:
            #reinitialising variable i
            i = -1
            while(True):
                i = i + 1
                if (i > len(ph)):
                    break
                else:
                    j = -1
                    while (True):
                        j = j + 1
                        if (j >= 26):
                            break
                        elif (cyc_d.B[i][1].upper() == cyc_d.B[j][0] and cyc_d.B[i][1] != ''):
                            # cyc.B(i,1) is user data class variable object array rank 2
                            # in the hyhoteses of not spacing
                            # assignement to rank output B(i,2) rank 2
                            # the decalated input of user by key ke
                            # mod26 the rest of division on 26
                            cyc_d.B[i][2] = cyc_d.B[j + 26 - k % 26][0]
                        elif (cyc_d.B[i][1] == ''):
                            # in the hyhoteses of spacing
                            cyc_d.B[i][2] = ' '  # cyc.B[i][1] THE spacing is '' for python reduced to empty
                            # not need to continue to analylsed another letters
                            break
                        # EndIf
                    # EndWhile
                    # print(cyc.B)
                #EndIf
            #EndWhile
            #print(cyc.B)
            # function to output the result to every try to key until a decyprated phrase
            print('with key', -(k), 'la decryptage is le suivante_:')
            output_array(ph, cyc_d)
        #EndIf
    #EndWhile
#EndFunctoin_decrypyte
#
#
#starting execution program array
if __name__=='__main__':
    main()
#EndIf