import numpy as np
#import dynarray as da


#creating a data structured derived type class moy, as template
class moy:
    #if you want use constant value to inner class
    #def ___init__(self):
    #    self.name='mahabarata'
    #    self.age=5500
    #data structured fields array variable multidimentional H integer et undefininited in rank and dimention dynamically
    #structured derived type array fields H of type integer 32
    #dynamically
    H = np.empty((),dtype=np.int32,order='F')
    # data structured fields array variable multidimentional M character et undefininited in rank and dimention dynamically
    # structured derived type array M of type character , not utilised
    #M = np.zeros((2, 6, 4), dtype=np.chararray, order='F')
#EndClass_moy

def main():
    #instancy of variable structured derived type to passing in dummy  argumenty to multiples functions
    mo=moy()
    f='cvs_file.csv'
    input_cvs(f)
    #function to output value en using the data structured derived type mo.H
    R=output_ocsv(f)
    #not need to repeat a calcul of function, assignement in one dimentiona array R the values of return
    print(R[0],R[1])
    #the return of output file .csv is C the construited array multi dimentional R[0] and the rank of fields R[1]
    mo=moyenne_stu(R[0],R[1]) #not you upgrading mo variable data structured derived type
    #assignement the construited variable structure derived type in mo
    print(mo)
    #defining function moyenne par matieres
    #dummy arguments
    #upgrading the structured derived type variable mo , but apparently not need too
    mo=moyenne_mat(R[0],R[1])
    #input the new data in file cvs by function input_acvs
    print(mo)
    #upgrading R[0] is the last value of variable structure derived type mo
    #automatically  upgrading
    input_acvs(f,R[0],R[1])
#end_main

#input_to_cvs file to append the new data
def input_acvs(fd,mo,n):
    #variable structured derived type mo upgranding is passed in dummy argument
    print(mo)
    print(n)
#End_function_input_acvs


#input to file.csv
def input_cvs(fd):
    mo = moy()
    # created an instancement of class moy
    print(mo.H)
    #sized undefinited dynamically data structured derived type array mo.H
    mo.H = np.zeros((3, 6, 4), dtype=np.chararray, order='F')
    print(mo.H)
    with open(fd,'w') as fo:
        #a emporary array for join command
        #utilised in this function
        G = np.empty((4),dtype=np.chararray,order='F')
#
        mo.H[0][0][0] = 'students'
        mo.H[0][0][1] = 'note math'
        mo.H[0][0][2] = 'note pyhsique'
        mo.H[0][0][3] = 'note info'
#
        mo.H[0][1][0]='bob'
        mo.H[0][2][0]='alice'
        mo.H[0][3][0]='jamkar'
        mo.H[0][4][0]='othman'
        mo.H[0][5][0]='francois'
#
        mo.H[0][1][1]='17'
        mo.H[0][2][1]='17'
        mo.H[0][3][1]='15'
        mo.H[0][4][1]='19'
        mo.H[0][5][1]='18'
#
        mo.H[0][1][2]='19'
        mo.H[0][2][2]='25'
        mo.H[0][3][2]='15'
        mo.H[0][4][2]='15'
        mo.H[0][5][2]='9'
#
        mo.H[0][1][3]='15'
        mo.H[0][2][3]='15'
        mo.H[0][3][3]='5'
        mo.H[0][4][3]='15'
        mo.H[0][5][3]='5'
#
        #
#
        print(mo.H)
        i=-1
        j=-1
        while(True):
            i=i+1
            if(i>5): # to found the first rank dimentional array
                break
            else:
                #temporary array one dimentional G[] to output in file .csv
                j=-1
                while(j<4-1):
                    j=j+1
                    G[j]=mo.H[0][i][j]
                    #fo.write('bob','13')
                #EndWhile
            s = ','.join(G) # join an one dimentional temporary array in string s to write in file.csv
            fo.writelines(s+'\n')
            #fo.write(s+'\n') #to return line in line by line in the file . csv
            #the effect is similary because is only one line in string s
            #EndIf
    #end_with
    return(mo)
#end_moyenne

#output o from file.csv
def output_ocsv(fd):
    mo=moy()
    #created an instancement of class moy
    print(mo.H)
    # sized undefinited dynamically data structured derived type array mo.H
    #meme si the data structured derived type is already utilisaed is scratch the
    #previous contents
    mo.H = np.zeros((3, 6, 4), dtype=np.chararray, order='F')
    #second dimention _ rank 6 is the First dimention for convention _
    #thirdy dimention _ rank 4 is the Second dimention for convetion _
    #first dimention _ rank 2 is the thirdy dimention for convention _
    print(mo.H)
    i=-1
    with open(file=fd,mode='r',encoding='utf-8') as fo:
        while(True):
            i=i+1
            s=fo.readline()#+'\n'  #read line by line with out assignement in one dimentional array to line, but to a temporary string
            #the EOF jointe
            if(not s):
                break
            #EndIf
            #fo.read command output interely file in one block
            #fo.readline is the read file line by line to splitting
            #fo.readlines is the reading to all lines and assignement intierely line in a one dimentional array : do not use is inefficienly
            #
            #not possibility to avoid a list inefficiently , the command split in ce python give an output , not a one dimentional array
            #temporary, but à list
            li=s.split(',')
            #not problem the last element with \n to final , it's not considered in output of element of one dimentional
            #temporary array
            print(len(li)) #to found the temporary one dimentional array li
            #assignement of element of temporary array one dimentional to multidimentional array C
            mo.H[0][i][0] = li[0] #the field name students type char
            mo.H[0][i][1] = li[1] #the field note math type double
            mo.H[0][i][2] = li[2] #the field note pyhsiqie type double
            mo.H[0][i][3] = li[3]  #the fields note info type double
            #C[0][i][4] = li[4]
            print(li)

            print(s)
        #EndWhile
    #EndWith
    #important: return intierely variable object mo data structured derived type,
    #not à only fields data structured derived type, if command return(mo.H,i) you
    #you missing the values, not recognized data structured derived type class.
    return(mo,i)
#EndFunction_moyenne_ocsv

#append file to output calcule numerical
#passing in dummy argument intierely data structured derived type variable mo
def moyenne_stu(mo,n):
    print('the dummy argument of rank of 1 dimention',n)
    #this an arrat F , but not data structured derived type?
    print(mo.H)
    sum=0
    #utilised the array dummy argument F
    i=0
    mo.H[1][i][0]='student'
    mo.H[1][i][1] = 'moyenne_stud_'
    while(True):
        i=i+1
        if(i>6-1):
            break
        else:
            j=0
            sum=0
            while(True):
                j = j + 1
                if(j>4-1):
                    break
                else:
                    p = int(mo.H[0][i][j], 10)
                    sum=sum+p
                #EndIf
            #EndWhile
            #second rank of F in thirdy dimention for output of calcule numerical
            #the name of students
            mo.H[1][i][0]=mo.H[0][i][0]
            #the middle of notes
            mo.H[1][i][1]=sum/(j-1)  #calcule
        #EndIf
        print('la moyenne for student',mo.H[1][i][0],'is :',mo.H[1][i][1])
    #endWhile
    return(mo)
#output in console and file .csv
print('--------------------------')


#endfunction

def moyenne_mat(mo,n):
    print(mo)
    print(mo.H)
    sum=0 #initializing empty value for sum
    i=0
    mo.H[2][i][0]='discipliny'
    mo.H[2][i][1] = 'moyenne disc_'
    while(True):
        i=i+1
        if(i>4-1):  #cond_1
            break
        else:
            j=0
            sum=0
            #in this part of program , in inner loop , you make the sum of
            #notes for discipline , and not for students
            while(True):
                j=j+1
                if(j>6-1):   #cond_2
                    break
                else:
                    p=int(mo.H[0][j][i],10)
                    sum=sum+p
                #EndIf       #cond_2
            #EndWhile
            mo.H[2][i][0]=mo.H[0][0][i]
            #the middle of note for discipliny
            mo.H[2][i][1]=sum/(j-1) #calcule of middle of notes for discipliny
        #EndIf  cond_1
        print('la moyenne for discipline', mo.H[2][i][0], 'is :', mo.H[2][i][2])
    #EndWhile
    return(mo)
#End_Function_moyenne_mat


#creating a new file to output of students, moyenne and result pass not pass
#def
#endfunction
#
#starting program
if __name__=='__main__':
    main()
#EndIf
