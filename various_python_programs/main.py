import numpy as np
def main():
    a=2
    b=2
    print('the results are :  ', a+b)
    try:
        #B=np.arange(0,15)
        #middle()
        #middle_saisi() #if need to call
        seaux() #if need to call
    except ImportError:
        print('founded error')
    #EndTry
#EndFunction_main
#
def middle_saisi():
    inf=1
    c=0
    i=0
    while(inf==1):
        i=i+1
        num=int(input('enter a number, -1 for exit'))
        if(num==-1):
            n=i-1
            break
        else:
            c=c+num
        #EndIf
    #EndWhile
    print('the middle is : ', c/n)
#EndProgram_middle_saisi

def middle():  #try to pass a array as dummy argument
#
    A=np.arange(0,15)
    sum=0
    i=-1
    while(True):
        i=i+1
        if(i>15-1):
            break
        else:
            sum=sum+A[i]
        #enfIf
    #EndWhile
    print('middle=',sum/i)
#EndFunction
#
#
def seaux():
    D = np.zeros((2,2)) #initialising zero array 2 dimentional , rank 2
    print(D) #for control
    i=0
    while(True):
        if(D[1][0]==4):
            print('the seau 2 is remply until :',D[1][0],'_litres')
            return(D)
        else:
            if(D[1][0]<5):
                vidan_remplis(1,'r', D)
            elif(D[1][0]==5):
                vidan_remplis(2,'v',D)
            #else:
            #EndIf
        vers(12,D)
        #EndIf
    #EndWhile
##
##notes:
    #remplissage seau 1  de 3litres
    #vidan_remplis(1,'r',D)
    #versage du 1 in 2
    #vers(12,D)
    #print(D)
    #il doit avoir 3 litres in seau 2
    #remplissage seau 1 de 3 litres a nouveau
    #vidan_remplis(1,'r',D)
    #print(D)
    #versage in de Seau 1 in seau 2 , jusqu"au limite de 5 litres
    #vers(12,D)
    #print(D)
    #il doit avoir 1 litre in seau 1
    #vindange du seau 2 de 5 litres
    #vidan_remplis(2,'v',D)
    #versage du seau 1 in seau 2
    #vers(12,D)
    #il doit avoir un litre in seau 2 et zero in seau 1
    #remplissage de seau 1
    #vidan_remplis(1,'r',D)
    #versage final du seau 1 de trois in seau 2
    #vers(12,D)
    #il doit avoir 0 litres in seau 1 et 4 litres in seau 2
#EndFunction
#
#
#function versange
#il sert pour verser l'eau d'un seau à l'autre et viceversa
#12 de 1 à seau 2 (seau 2 de 5 litres, l'element of array C[1])
#21 de 2 à seau 1  (seau 1 de 3 litres, l'element of array C[0])
#passing dummy argument array C by ref
def vers(sens,C):
    i=1
    while(True):
        i=i+1
        #case in the sens 1 -> 2
        if (sens==12 and (C[1][0]>=5 or C[0][0]==0)):
            print(C)
            break
        elif(sens==12):
            C[1][0]=C[1][0]+1 #remplissage du seau 2 , C[1]
            C[0][0]=C[0][0]-1 #vider le seau 1, C[0]
        #EndIf
        #case in the sens 2->1
        if (sens==21 and (C[0][0]>=3 or C[1][0]==0)):
            print(C)
            break
        elif(sens==21):
            C[0][0]=C[0][0]+1 #remplissage du seau 1 , C[0]
            C[1][0]=C[1][0]-1 #vider le seau 2, C[1]
        #EndIf
    #EndWhile
    return(C)
#EndFunction_vers
#
#function vidange_remplis
#il sert pour vider le seau 1 ou 2 in dummy argument
#s=1 le seau est l'1
#si s=3 , on le remplis ou vide touts le deux
#rv=r on replisse
#rv=v on vide
def vidan_remplis(s,rv,E):
    if(rv == 'v' and s != 3):
        E[s-1][0]=0
    elif(rv == 'v' and s == 3):
        E[s-2][0]=0
        E[s-3][0]=E[s-2][0]
    #EndIf
    if (rv == 'r' and s != 3):
        if(s==1):
            E[s - 1][0] = s + 2 #remplissage seau 3 litres
        elif(s==2):
            E[s - 1][0] = s + 3 #remplissage seau 5 litres
        #EndIf
    elif (rv == 'r' and s == 3):
        E[s - 2][0]= 5
        E[s - 3][0] = 3
    #EndIf
    return(E)
#EndFunction_vidange
##
#
if __name__=='__main__':
    main()
#EndIf
#
#SEE too the exercises in the exercise paper in the folder

#10.10 the next program calculate à order of number of one dimentional array in order croissant

#15.10 the next program calculate the alphabet in word in order biggest , with inner loop
#bubble sort


