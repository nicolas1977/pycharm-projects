import numpy as np

def main():
    f='cvs_file.csv'
    moyenne_cvs(f)

#end_main

def moyenne_cvs(fd):
    with open(fd,'w+') as fo:
        A=np.zero((5,3))
#        
        A[0][0]=['bob','13','22']
        A[1][0]=['alice','15','10']
        A[2][0]=['bob','13','15']
        A[3][0]=['othman','17','15']
        A[4][0]=['francois','17','15']
#
        A[0][1]=['17']
        A[1][1]=['17']
        A[2][1]=['15']
        A[3][1]=['19']
        A[4][1]=['18']
#
        A[0][2]=['19']
        A[1][2]=['25']
        A[2][2]=['15']
        A[3][2]=['15']
        A[4][2]=['9']
#
        A[0][3]=['15']
        A[1][3]=['15']
        A[2][3]=['5']
        A[3][3]=['15']
        A[4][3]=['5']
#
        l=[]
        i=-1
        while(True):
        #
            if(i>4): # to found the first rank dimentional array
                break
            else:
                #temporary array one dimentional l to output in file .csv
                while(i>4):
                    i=i+1         
                    l.append(A[i][0])                
                    s=','.join(l)
                    fo.write(s)
                    #fo.write('bob','13')
            #end_with    
                
#end_moyenne


#starting program
if __name__=='__main__':
    main()
#EndIf
