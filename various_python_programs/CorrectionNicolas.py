import numpy as np

# mainprogram
def main():
    try:
        ty=str(input('enter the type of graphe : "oriented" or "not_oriented"'))
        if(ty=='oriented' or ty=='not_oriented'):
            print('the program create the matrix of your choix')
        else:
            print('you must decided of "oriented" or "not_oriented" value; try again , restarting program')
            exit()
        #EndIf
        M = create_alternating_matrix(5,ty)
        print("I will analyse this matrix of size ", np.shape(M))
        print(M)
        analyse_symmetry(M)
        cho=str(input('do you want found the "arrets" or "arcs"?'))
        while(True):
            if(cho=='arcs'):
                print('the sum of ', cho,' are=_', arc_arr(M,cho))
                break
            elif(cho=='arrets'):
                 #calling function arc_arr in che case arrets
                print('the sum of ', cho,' are=_', arc_arr(M, cho))
                break
            else:
                print('not understood the answers, please enter "arcs" or "arrets"...')
                cho = str(input('...'))
            #EndIf
        #EndWhile
    except ImportError:
        print('founded error')
# endprogram
       

# Analyse symmetry, without double operation en plus
def analyse_symmetry(M):
    size=np.shape(M)[0]
    u = 0
    while(u<=size-2):
        v = u + 1
        while(v<=size-1):
            if(M[u][v]!=M[v][u]):                
                print('The matrix is not totally symmetric')
                return(False)
            #EndIf
            v = v + 1
        #EndWhile
        u = u + 1
    #EndWhile
    print('The matrix is totally symmetric')
    return(True)    
#EndFunction_analyse_symmetry

#computing arcs_arrets in input dummy argument M array
#selon the user's demand
def arc_arr(M,dem):
        siz=np.shape(M)[0]
        print(siz) #for control dimention rank-0
        #initializing the values arc and arrets and sum value for compteur
        arc=0
        arr=0
        sum=0
        u=-1
        while(u<siz-1):  #Do...Loop_1
            u=u+1
            if(dem=='arcs'):
                #l'analyse vais faites depuis le premieres 'columns' for each iteration
                #on est pas sur que il y a symmetrie, et pour la diagonale , quelque elements of
                #array peut etre en relation avec lui meme. On les analyse touts les éléments, comme de vous indiqué,
                #so on reinitialising value v in -1 for each iteration , in the case graph orienté
                v=-1
            elif(dem=='arrets'):
                #etant le graphe non orienté toujour symmetrique , on evite de analyser les elements symmetriques,
                #et diagonale excluse, etant aucun element en relation avec lui meme
                v=u
            #EndIf
            while(v<siz-1):  #Do..Loop_2
                v=v+1 #first iteration, v=1, second, v=2, ..., last, v=siz-1
                #in the last iteration v value is siz-1, before the command while
                if(M[v][u]==1):
                    sum=sum+1
                #EndIf
            #EndWhile_2
        #EndWhile_1
        return(sum)
#EndFunction
#
# Create a matrix of size n*n alternating columns of 0 and columns of 1
# following the choix of user , if it will be non oriented , and not forcement symmetrique
# or oriented et donc symmetrique
def create_alternating_matrix(n,type):
    B=np.zeros((n,n))
    #print(B)    
    i=-1
    #type='oriented' testing steps
    #type='not_oriented' testing step
    while(True):
        i=i+1
        if(i>n-1):
            break
        #EndIf

        j=-1
        while(True):
            if(j>=n-2):
                break
            #EndIf
            j=j+2
            if(type=='oriented'):
                B[i][j]=1
            elif(type=='not_oriented'):
                if (i == j):
                    #the diagonal in not_oriented graph , jamais un elements est en relation avec itself
                    B[i][j] = 0
                else:
                    #c'est probable que le programme effectue le calcule en double, mais je n'ai pas le temps pour le modifier parce que faisse une seule operation, en creant la demi array symmetrique
                    B[i][j]=1
                    B[j][i]=1
                #EndIf
            #EndIf
        #EndWhile
    #EndWhile   
    #print(B)
    return(B)
#EndFunction_create_alternating_matrix

if __name__=='__main__':    
    main()
#EndIf
