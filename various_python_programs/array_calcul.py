import numpy as np
print('the vesion numpy is:', np.__version__)
# mainprogram
def main():
    try:
        B=np.ones((2,4,3)) #passing in dummy argument of function array_dummy_arg following
        print(B)
        # calcule_arr(10) #suspens if need
        array_dummy_arg(B)
    except ImportError:
        print('founded error')
    #EndTry
#Endprogram
#
# CONTAINS FUNCTION calling from main program main:
#
# tranforming the character in integer selon la position alphabetique
def calcule_arr(n1):
    #
    A = np.zeros((n1, 3, 2))  # .reshape(4,2,3)#setting undefinited numpy array not empty 3 dimentional, until zeros of empty array
    # attention is the n element of array with not Option Base 1 , so the lim sup  for each the dimentional
    # will be A[n-1][n-1][n-1] , in this case A[3][4][2]
    i = -1   #assignement value in 3 dimentionna array a
    j = -1
    k = -1
    # the command continue cause , of the initial part of succession conditional inner loop concerned,
    # as CYCLE in FORTRAN, but not continue to nexted do...loop; not adapted for this case
    while (True):
        i = i + 1
        if (i > (10 - 1)):
            break  #stopping only the do...loop concerned, not the another
        else:
            j=-1
            while(True):
                j = j + 1
                if (j>(3-1)):
                    break
                else:
                    k=-1 # don't forget to reinitialized variable for the next do...loop_3
                    while (True):
                        k = k + 1
                        if(k>(2-1)):
                            break
                        else:
                          A[i][j][k]=i*2+1
                        #Endif_cond_3
                    #endwhile_3
                #Endif_cond_2
            #EndIf_cond_2
        #EndIf_cond_1
    #EndWhile_1
    print('output of main program:_', A)
    print('output of main program:_', A.shape)
    print('output of main program:_', A[3][1][0])
# endfunction_calcule_arr
#
#
def array_dummy_arg(H):
    print('the array passed by ref in dummy argument is:',H)
    print('the shape of passing dummy argument is=',H.shape)
#EndFunction_array_dummy_arg
#


if __name__ == '__main__':  #the starting step of main program
    main()
#EndIf