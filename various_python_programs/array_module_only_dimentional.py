#creating array only one dimentionally without numpy
#on peut utiliser aussi #from array import *
#and after user A = array('i",[]) , without repetition of ar.array
# ar. object
import array as ar
import sys

#print('the vesion numpy is:', np.__version__)
#
#
# mainprogram
def main():
    try:
        A = ar.array('i', [1,5,3,45]) #creating en empty integer array
        #B = ar.array([[1,5],
                      #[1,2,5,3],
                      #[1,21,21,25]])  # impossible creating  Three dimentional array
                       # python
        C = ar.array('d', [])  # creating en empty double p array
        D=ar.array('f',[])
        # array
        print(A)
        print(C)
        print(D)
        print (A[0],A[2],D.append(2.5))
        # calcule_arr(10) #suspens if need
        array_dummy_arg(A,D,C)
    except ImportError:
        print('founded error')
    #EndTry
#Endprogram
#
# CONTAINS FUNCTION calling from main program main:
#
# tranforming the character in integer selon la position alphabetique
def array_dummy_arg(A,D,C):
    print(A)
    print(D)
    print(C)
#End array dummy arg
#
if __name__ == '__main__':  #the starting step of main program
    main()
#EndIf