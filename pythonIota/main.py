# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
import numpy as np


def main():
    # Use a breakpoint in the code line below to debug your script.


    valName=input('Enter a name: ')  # Press ⌘F8 to toggle the breakpoint.


    #array of adhiacence
    #(0,1)=(pierre, sophie)->1
    #0=pierrer
    #1=sophie
    #2=arnauld
    #3=lise
    #4=hugo
    #5=Romain

    A=np.zeros((6,6),dtype=np.int,order='F')

    # array of visited sommet
    B = np.zeros((60, 2), dtype=np.int, order='F')

    # array of adhiacence
    A[0][1]=1
    A[0][2]=1
    A[1][3]=1
    A[1][4]=1
    A[2][5]=1

    # first string of stringGlobal
    stringGlobal: str = valName

    # analyse parcours graphe par i in twoChildren





    print(A)
    i: int = startSommetGraph(valName)
    j: int = 0
    found: bool = False
    present: bool = False
    # isPresent(childrens(A, B, i),i)
    while(i<len(A)):
        present=isPresent(childrens(A, B, i), i)
        while(j<len(A)):
            if(A[i][j]==1 and (present or i==startSommetGraph(valName))):
                found=True
                # print(A[i][j])
                stringGlobal = stringGlobal +"_"+ outputName(j)
                # print(outputName(j))
            j=j+1
        # End Do ..Loop 1
        j=0
        i=i+1
    # End Do .. Loop 2
    print("---------------------------------------------------")
    if(found==True):
        print("the value is :_"+stringGlobal)
    else:
        print("the value is :__with this name_"+stringGlobal+"_not hierachical "
                                                             "values inferior founded")
    # end if
# End main


def outputName(n):
    if (n==0):
            return "pierre"
    elif(n==1):
            return "sophie"
    elif (n == 2):
            return "arnauld"
    elif (n == 3):
           return "lise"
    elif (n == 4):
            return "huong"
    elif (n == 5):
        return "romain"
#end funnc puputname

def startSommetGraph(name):
    if(name=='pierre'):
        return 0
    if(name=='sophie'):
        return 1
    if(name=='arnaud'):
        return 2
    if(name=='lise'):
        return 3
    if(name=='hugo'):
        return 4
    if(name=='romain'):
        return 5
    else:
        return 6
# end func StartSommetGraph


def childrens(A, B, sommetRelatif):

    j:int=0
    # le first sommet relarif is itsself

    k=B[0][1]
    # B[k][0] = sommetRelatif

    while(j<len(A)):
        if(A[sommetRelatif][j]==1):
            B[k][0]=j
            k = k + 1
        # EndIf
        j=j+1
    # EndWhile

    # index of last sommet recorded
    B[0][1]=k

    # un temporary array for indicate the two children (if graphe binaire)
    return B
# End Children

def isPresent(B,sommetActual):
    j:int=0
    while(j<len(B)):
        if(sommetActual==B[j][0]):
            return True
        # end if
        j=j+1
    # end Do .. loop
    return False
# End Func isPresent

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
#END IF
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
